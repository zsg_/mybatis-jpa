package com.woodws;

import com.woodws.mybatis.samples.Application;
import com.woodws.mybatis.samples.dao.FuncMapper;
import com.woodws.mybatis.samples.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class ApplicationFuncTests {

    @Autowired
    private FuncMapper funcMapper;


    @Test
    @Commit
    public void testMapper() throws Exception {
        String s = funcMapper.funcTestFunc("111","222");
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("id",38);
        map.put("name",null);
        User user = funcMapper.callTestProcedure(map);
        System.out.println("----------------" + s);
        System.out.println("-------name---------" + map.get("name"));
        System.out.println("----------------" + user.getName());
    }

}
